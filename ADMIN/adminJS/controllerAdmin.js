const URL_BASE = "https://63f442fcfe3b595e2ef03e59.mockapi.io";
// GET LIST AXIOS
export let getListAxios = () => {
  return axios({
    url: `${URL_BASE}/products`,
    method: "GET",
  });
};

// GET AXIOS ID
export let getIDAxios = (id) => {
  return axios({
    url: `${URL_BASE}/products/${id}`,
    method: "GET",
  });
};

// DELETE
export let deleteIDAxios = (id) => {
  return axios({
    url: `${URL_BASE}/products/${id}`,
    method: "DELETE",
  });
};

// CREATE
export let createDataAxios = (data) => {
  return axios({
    url: `${URL_BASE}/products`,
    method: "POST",
    data: data,
  });
};
// UPDATE DATA AXIOS
export let updateDataAxios = (id, data) => {
  return axios({
    url: `${URL_BASE}/products/${id}`,
    method: "PUT",
    data: data,
  });
};

// RENDER DATALIST AXIOS
export let renderData = (array) => {
  let contentHTML = "";
  for (let i = 0; i < array.length; i++) {
    let item = array[i];
    contentHTML += `
  <tr>
    <td>${item.id}</td>
    <td class="font-weight-bold">${item.name}</td>
    <td>${item.price}</td>
    <td>${item.type}</td>
    <td><img src=${item.img} style="width:50px"></td>
    <td><button class="btn btn-secondary" onclick="deleteProduct(${item.id})">DELETE</button><button class="btn btn-warning ml-2"data-toggle="modal"
    data-target="#exampleModalCenter" onclick="fixProduct(${item.id})">FIX</button></td>
  </tr>
  `;
  }
  document.getElementById("tbodyContent").innerHTML = contentHTML;
};

// VALIDATE ID
export let valiID = () => {
  let id = document.getElementById("id").value;
  document.getElementById("tbID").style.color = "red";
  let re = /^[0-9]*$/;
  if (id == "") {
    document.getElementById("tbID").innerHTML = "Vui lòng nhập mã ID";
    return false;
  }
  if (re.test(id)) {
    document.getElementById("tbID").innerHTML =
      "Lưu ý: Không thể thay đổi ID khi chỉnh sửa";
    return true;
  } else {
    document.getElementById("tbID").innerHTML = "ID bao gồm số nguyên";
    return false;
  }
};

// VALIDATE NAME
export let validateName = () => {
  let name = document.getElementById("name").value;
  document.getElementById("tbName").style.color = "red";
  let re = /^([a-zA-Z0-9 _-]+)$/;
  if (name == "") {
    document.getElementById("tbName").innerHTML = "Vui lòng nhập tên sản phẩm";
    return false;
  }
  if (re.test(name)) {
    document.getElementById("tbName").innerHTML = "";
    return true;
  } else {
    document.getElementById("tbName").innerHTML =
      "Bạn nhập chưa đúng, sản phẩm bao gồm chữ và số nguyên";
    return false;
  }
};

// VALIDATE PRICE
export let valiPrice = () => {
  let id = document.getElementById("price").value;
  document.getElementById("tbPrice").style.color = "red";
  let re = /^[0-9]*$/;
  if (id == "") {
    document.getElementById("tbPrice").innerHTML = "Vui lòng nhập số tiền";
    return false;
  }
  if (re.test(id)) {
    document.getElementById("tbPrice").innerHTML = "";
    return true;
  } else {
    document.getElementById("tbPrice").innerHTML = "Số tiền bao gồm chố";
    return false;
  }
};

// VALIDATE NAME
export let valiType = () => {
  let name = document.getElementById("type").value;
  document.getElementById("tbType").style.color = "red";
  let re = /^([a-zA-Z0-9 _-]+)$/;
  if (name == "") {
    document.getElementById("tbType").innerHTML =
      "Vui lòng nhập tên loại sản phẩm";
    return false;
  }
  if (re.test(name)) {
    document.getElementById("tbType").innerHTML = "";
    return true;
  } else {
    document.getElementById("tbType").innerHTML =
      "Bạn nhập chưa đúng, loại sản phẩm bao gồm chữ và số nguyên";
    return false;
  }
};
