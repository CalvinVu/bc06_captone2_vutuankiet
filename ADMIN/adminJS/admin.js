import {
  renderData,
  getListAxios,
  getIDAxios,
  createDataAxios,
  deleteIDAxios,
  updateDataAxios,
  validateName,
  valiID,
  valiPrice,
  valiType,
} from "./controllerAdmin.js";
import { Product, ProductPrice } from "./modal.js";
import { getValueInput } from "./modal.js";
// RENDER LIST DATA FROM AXIOS
let fetch = () => {
  getListAxios()
    .then((res) => {
      console.log(res);
      renderData(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};
fetch();

// ADD PRODUCT
let addProductToAxios = () => {
  let product = getValueInput();
  let isVali = validateName();
  isVali = isVali & valiID();
  isVali = isVali & valiPrice();
  isVali = isVali & valiType();
  if (isVali) {
    createDataAxios(product)
      .then((res) => {
        console.log(res);
        fetch(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }
};
window.addProductToAxios = addProductToAxios;

// REMOVE PRODUCT
let deleteProduct = (id) => {
  deleteIDAxios(id)
    .then((res) => {
      console.log(res);
      fetch(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};
window.deleteProduct = deleteProduct;

// FIX PRODUCT
let fixProduct = (id) => {
  getIDAxios(id)
    .then((res) => {
      console.log(res);
      document.getElementById("id").value = res.data.id;
      document.getElementById("name").value = res.data.name;
      document.getElementById("price").value = res.data.price * 1;
      document.getElementById("screen").value = res.data.screen;
      document.getElementById("backCamera").value = res.data.backCamera;
      document.getElementById("frontCamera").value = res.data.frontCamera;
      document.getElementById("desc").value = res.data.desc;
      document.getElementById("type").value = res.data.type;
    })
    .catch((err) => {
      console.log(err);
    });
};
window.fixProduct = fixProduct;

// UPDATE PRODUCT
let updateProduct = () => {
  let isVali = validateName();
  isVali = isVali & valiID();
  isVali = isVali & valiPrice();
  isVali = isVali & valiType();
  let product = getValueInput();
  console.log(product);
  console.log(product.id);
  if (isVali) {
    updateDataAxios(product.id, product)
      .then((res) => {
        console.log(res);
        fetch(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }
};
window.updateProduct = updateProduct;

// SEARCH PRODUCT
let searchProduct = () => {
  let inforSearch = document.getElementById("inputSearch").value;
  getListAxios()
    .then((res) => {
      console.log(res);
      let data = res.data.map((item) => {
        return new Product(
          item.id,
          item.name,
          item.price,
          item.screen,
          item.backCamera,
          item.frontCamera,
          item.img,
          item.desc,
          item.type
        );
      });
      console.log(data);
      let find = data.filter((item) => {
        return (
          item.name.toLowerCase().trim() == inforSearch.toLowerCase().trim()
        );
      });
      console.log(find);
      renderData(find);
    })
    .catch((err) => {
      console.log(err);
    });
};
window.searchProduct = searchProduct;

// UP PRICE
let cleanUpPrice = () => {
  console.log("hihi");
  getListAxios()
    .then((res) => {
      console.log(res);
      let data = res.data;
      for (let i = 0; i < data.length - 1; i++) {
        for (let j = i + 1; j < data.length; j++) {
          if (data[j].price < data[i].price) {
            let t = data[i];
            data[i] = data[j];
            data[j] = t;
          }
        }
      }
      console.log(data);
      renderData(data);
    })
    .catch((err) => {
      console.log(err);
    });
};
window.cleanUpPrice = cleanUpPrice;

// DOWN PRICE
let cleanDownPrice = () => {
  getListAxios()
    .then((res) => {
      console.log(res);
      let data = res.data;
      for (let i = 0; i < data.length - 1; i++) {
        for (let j = i + 1; j < data.length; j++) {
          if (data[j].price > data[i].price) {
            let t = data[i];
            data[i] = data[j];
            data[j] = t;
          }
        }
      }
      console.log(data);
      renderData(data);
    })
    .catch((err) => {
      console.log(err);
    });
};
window.cleanDownPrice = cleanDownPrice;

// //VALIDATION USERNAME
// function valiUser() {
//   document.getElementById("tbTen").style.display = "block";
//   var name = document.getElementById("name").value;
//   var re = /^[a-zA-Z\s]*$/;
//   if (name == "") {
//     document.getElementById("tbTen").innerHTML = "sai";
//     return false;
//   }
//   if (re.test(name)) {
//     document.getElementById("tbTen").innerHTML = "";
//     return true;
//   } else {
//     document.getElementById("tbTen").innerHTML = "sai";
//     return false;
//   }
// }
// // CAP NHAT SINH VIEN
// document.getElementById("btnCapNhat").onclick = function () {
//   var nhanVien = layValueTuForm();
//   var viTri = danhSachNhanVien.findIndex(function (item) {
//     return item.taiKhoan == nhanVien.taiKhoan;
//   });
//   var isVali = valiTaiKhoan();
//   isVali = isVali & valiEmail();
//   isVali = isVali & valiPassword();
//   isVali = isVali & valiUser();
//   isVali = isVali & valiLuongCoBan();
//   isVali = isVali & valiChucVu();
//   isVali = isVali & valiGioLam();
//   if (isVali == true) {
//     danhSachNhanVien[viTri] = nhanVien;
//     // NGUY HIEM NEU NGUOC LAI
//     renderNhanVien(danhSachNhanVien);
//     localDSSV();
//   }
// };
