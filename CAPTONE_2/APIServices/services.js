const BASE_URL = "https://63f442fcfe3b595e2ef03e59.mockapi.io";

// GET LIST
export let getListAxios = () => {
  return axios({
    url: `${BASE_URL}/products`,
    method: "GET",
  });
};

// GET PRODUCT BY ID
export let getProductByID = (id) => {
  return axios({
    url: `${BASE_URL}/products/${id}`,
    method: "GET",
  });
};
