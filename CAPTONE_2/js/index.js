import { renderProduct, filter } from "./controller.js";
import { getListAxios, getProductByID } from "../APIServices/services.js";
import { Product } from "./modal.js";

let fetchProductList = () => {
  getListAxios()
    .then((res) => {
      let dataList = res.data;
      renderProduct(dataList);
    })
    .catch((err) => {
      console.log(err);
    });
};
fetchProductList();

let filterTypeProduct = () => {
  getListAxios()
    .then((res) => {
      console.log(res);
      filter(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};
window.filterTypeProduct = filterTypeProduct;

export let cart = [];
console.log(cart);

// GET DATA LOCAL
let dataJSON = localStorage.getItem("DATA");
if (dataJSON) {
  let dataUse = JSON.parse(dataJSON);
  cart = dataUse.map((item) => {
    return new Product(item.id, item.img, item.name, item.price, item.quantity);
  });
}

let addItem = (id) => {
  console.log("hihi");
  getProductByID(id)
    .then((res) => {
      console.log(res);
      let product = new Product(
        res.data.id,
        res.data.img,
        res.data.name,
        res.data.price
      );
      console.log(product);
      let index = cart.findIndex((item) => {
        return item.id == product.id;
      });
      console.log(index);
      if (index == -1) {
        cart.push(product);
        product.quantity = 1;
      } else {
        cart[index].quantity++;
      }
      moveDataJson();
    })
    .catch((err) => {
      console.log(err);
    });
};

window.addItem = addItem;
// TOTAL PRICE
let totalPrice = (quantity, price) => {
  return quantity * price;
};

// RENDER SHOPPING CART
let renderShoppingCart = () => {
  let contenHTML = "";
  let total = 0;
  for (let i = 0; i < cart.length; i++) {
    let item = cart[i];
    console.log(item);
    total += totalPrice(item.quantity, item.price);
    contenHTML += `
          <tr>
              <div class="row border m-auto">
                  <div class="col-4"><td><img src="${
                    item.img
                  }" class="w-50"></td></div>
                  <div class="col-8">
                  <td>ID:${item.id}</td>
                      <td>NAME: ${item.name}</td>
                      <td>PRICE: ${item.price
                        .toFixed(1)
                        .replace(/\d(?=(\d{3})+\.)/g, "$&,")}</td></br>
                      <td>QUANTITY:<button class="btn btn-success" onclick="upDownQuantity(${
                        item.id
                      }, -1)">-</button><strong> ${
      item.quantity
    } </strong><button class="btn btn-danger" onclick="upDownQuantity(${
      item.id
    }, +1)">+</button><button class="btn btn-warning ml-5" onclick="moveItemShopping(${
      item.id
    })"><i class="fa-solid fa-trash"></i></button></td>
                  </div>
              </div>
              <div class="">${item.quantity} * ${item.price
      .toFixed(1)
      .replace(/\d(?=(\d{3})+\.)/g, "$&,")} = ${totalPrice(
      item.quantity,
      item.price
    )
      .toFixed(1)
      .replace(/\d(?=(\d{3})+\.)/g, "$&,")} $</div>
          </tr>
          `;
  }
  document.getElementById("modalBody").innerHTML = contenHTML;
  document.getElementById("totalPrice").innerHTML = total;
};
window.renderShoppingCart = renderShoppingCart;

// UP QUANTITY
function upDownQuantity(id, count) {
  console.log(id);
  let position = cart.findIndex((item) => {
    return item.id == id;
  });
  cart[position].quantity = cart[position].quantity + count;
  cart[position].quantity == 0 && cart.splice(position, 1);
  renderShoppingCart(cart);
}
window.upDownQuantity = upDownQuantity;

// LOCAL STORED
let moveDataJson = () => {
  let dataStored = JSON.stringify(cart);
  localStorage.setItem("DATA", dataStored);
};

// PAY
let pay = () => {
  cart = [];
  console.log(cart);
  renderShoppingCart(cart);
};
window.pay = pay;

//CLEAR
document.getElementById("clear").addEventListener("click", function clear() {
  cart.length = 0;
  renderShoppingCart(cart);
  moveDataJson();
});

// MOVE ITEM SHOPPING
let moveItemShopping = (id) => {
  let index = cart.findIndex((item) => {
    return item.id == id;
  });
  console.log(index);
  cart.splice(index, 1);
  renderShoppingCart(cart);
  moveDataJson();
};
window.moveItemShopping = moveItemShopping;
