export let renderProduct = (dataRender) => {
  let contentHTML = "";
  for (let i = 0; i < dataRender.length; i++) {
    let itemData = dataRender[i];
    contentHTML += `
            <div class="card text-center col-3" style="width: 18rem">
      <img src="${itemData.img}" class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title">${itemData.name}</h5>
        <p>${
          itemData.desc.length > 10
            ? itemData.desc.slice(0, 20) + "..."
            : itemData.desc
        }</p>
        <p class="card-text">${itemData.price} $</p>
        <button class="btn btn-success" onclick="addItem(${
          itemData.id
        })" >ADD</button>
      </div>
    </div>
        `;
  }
  document.getElementById("renderProducts").innerHTML = contentHTML;
};
window.renderProduct = renderProduct;

export let filter = (itemList) => {
  let selectOption = document.getElementById("selectPhone");
  if (selectOption.value == "") {
    renderProduct(itemList);
  } else {
    let filterSameType = itemList.filter((item) => {
      return item.type.toUpperCase() === selectOption.value.toUpperCase();
    });
    renderProduct(filterSameType);
  }
};
window.filter = filter;
